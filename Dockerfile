FROM openjdk:8-jdk-alpine
LABEL maintainer="Glaud"
VOLUME /tmp
EXPOSE 8761
ARG JAR_FILE=/target/config-service-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} config-service.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/config-service.jar"]